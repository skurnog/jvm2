import java.util.Map;
import java.util.TreeMap;

public class StatisticUtil {

    private static final String SPACES = "    ";
    private static final Map<String, Long> statistics = new TreeMap<String, Long>();

    public static void incrementInstructionCounter(String instruction) {
        Long instructionCounter = statistics.get(instruction);
        if(instructionCounter == null) {
            statistics.put(instruction, 1L);
        } else {
            statistics.put(instruction, instructionCounter + 1L);
        }
    }

    public static void createShutdownHook() {
        Runtime.getRuntime().addShutdownHook(new Thread() {

            @Override
            public void run() {
                prepareAndPrintStatistics();
            }
        });
    }

    private static void prepareAndPrintStatistics() {
        for(Map.Entry<String, Long> entry : statistics.entrySet()) {
            String key = entry.getKey();
            Long value = entry.getValue();
            if(value >= 5) {
                System.out.println(key.toUpperCase() + SPACES + value.toString());
            }
        }
    }
}
