import org.apache.bcel.generic.*;
import org.apache.bcel.Repository;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.Constants;


import java.io.IOException;
import java.util.Iterator;

public class Transform {

    private static ClassGen classGen;
    private static ConstantPoolGen constantPoolGen;
    private static JavaClass clazz;

    public static void main(String[] args) {
        try {
            transform(args[0].replace(".class", ""));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void transform(String className) throws IOException, ClassNotFoundException {
        clazz = Repository.lookupClass(className);
        classGen = new ClassGen(clazz);
        constantPoolGen = classGen.getConstantPool();

        Method[] methods = clazz.getMethods();
        for (Method method : methods) {
            String methodName = method.getName();
            if(methodName.equals("<init>") || methodName.equals("<clinit>") ||
                    (methodName.startsWith("m") && !methodName.equals("main"))) {
                continue;
            }

            MethodGen methodGenerator = new MethodGen(method, classGen.getClassName(), constantPoolGen);
            InstructionFactory factory = new InstructionFactory(classGen);

            InstructionList instructionList = methodGenerator.getInstructionList();
            Iterator<InstructionHandle> it = instructionList.iterator();

            if(method.getName().equals("main")) {
                instructionList.insert(factory.createInvoke(StatisticUtil.class.getCanonicalName(), "createShutdownHook",
                        Type.VOID, Type.NO_ARGS, Constants.INVOKESTATIC));
            }

            while (it.hasNext()) {
                InstructionHandle instructionHandle = it.next();
                if(isInstructionExternalCall(instructionHandle)) {
                    continue;
                }

                instructionList.insert(instructionHandle, factory.createConstant(instructionHandle.getInstruction().getName()));
                instructionList.insert(instructionHandle, factory.createInvoke(StatisticUtil.class.getCanonicalName(), "incrementInstructionCounter",
                        Type.VOID, new Type[]{Type.STRING}, Constants.INVOKESTATIC));
            }

            methodGenerator.setMaxStack();
            methodGenerator.setMaxLocals();
            classGen.replaceMethod(method, methodGenerator.getMethod());
        }

        String classFilePath = Repository.lookupClassFile(classGen.getClassName()).getPath();
        classGen.getJavaClass().dump(classFilePath);
    }

    private static boolean isInstructionExternalCall(InstructionHandle instructionHandle) {
        Instruction instruction = instructionHandle.getInstruction();

        if(!(instruction instanceof InvokeInstruction)) {
            return false;
        }

        InvokeInstruction invokeInstruction = (InvokeInstruction) instruction;
        for(Method method : clazz.getMethods()) {
            if(classGen.getClassName().equals(invokeInstruction.getClassName(constantPoolGen).toString())
                    && method.getName().equals(invokeInstruction.getMethodName(constantPoolGen))
                    && method.getSignature().equals(invokeInstruction.getSignature(constantPoolGen))) {
                return false;
            }
        }
        return true;
    }
}
